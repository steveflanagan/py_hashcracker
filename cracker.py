from hasher import *
from guesser import *
from stalker import *

import sys

# --- lets get this party started --- #

# 0. Get info
sys_target = sys.argv[1]
try:
    sys_type = sys.argv[2]
except IndexError:
    sys_type = determine_hash_type(sys_target)

# 1. try 10M most common pwds from:
# github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-1000000.txt
common_pwds_txt = open("./common_pwds.txt", 'r')
common_pwds = [line[:len(line)-1] for line in common_pwds_txt.readlines()]
print(try_list(sys_target, sys_type, common_pwds))

# 2. try brute force up to 4 chars, lower up to 5, and numeric up to 6
# TODO what other brute forcing would make sense:
#   Some way to do Capital letter + some lower + some numbers/symbols. bet we could get up to 6 or 7 chars.
# TODO determine whether doing all_letters up to 5, upper up to 5, or alphanumeric up to 5 is worth it
# TODO somehow skip this when min pwd length reqs for the hash's source makes them impossible
# TODO determine whether each of these max sizes are worth it
# TODO somehow skip repeats - ie, brute_all(1-4) evidently covers brute_lc(1-4) and brute_numeric(1-4)
print(brute_all(sys_target, sys_type, 4))
print(brute_lowercase(sys_target, sys_type, 5))
print(brute_numeric(sys_target, sys_type, 6))

# 3. personalised attack
#   a. guess pwds of single interests
# TODO get this from scraping social media via get_interests
target_interests = ['football', 'jazz', 'boston']
for t in target_interests:
    # TODO consolidate this into one func in guesser
    print(check_appending_all_non_letters(t, sys_target, sys_type, 3))
    print(check_appending_birthyears(t, sys_target, sys_type))
    print(check_leet(t, sys_target, sys_type))
    # TODO start combining interests to make more passwords
    # TODO start guessing more interests from the initial list of interests
#   b. guess pwds of combining interests

# 4. hashcracking failed, get on the phone and social engineer this one hoss
