# TODO honestly a lot of this code can be repurposed as valuable in its own right
#   can be used for targetted advertising
#   effective scraping can search for more interesting data than simple interests...
#   amassing a large database can be used for making some interesting ML models

# generates a list of likely pwds for a person based on detected interests
def get_keywords(email):
    # TODO
    # 1. get a username for linkedin, insta, twitter, and fb for this email
    #   TODO worst case scenario, if this isn't possible, just pass this func
    #   those unames directly you lazy fuck lol
    # 2. amalagamate keywords scraped from these sources, including:
    #   a. places - city, address, hometown/homenation, emotionally significant places
    #   b. fanships - favourite sports team, food, colour, video game, book
    #   c. activities - music genres, sports to play, hobbies
    #   d. associations - clubs, schools, religious groups, workplace
    #   e. names - himself, family, friends, idols, athletes, musicians, authors
    #   f. TODO what else do people put in their pwds
    # 3. sort them by likelihood in password
    # 4. group keywords relating to each other into categories
    #   TODO ^ how?
    # 5. more ambitiously, guess even more interests from these gathered ones:
    #   a. scrape search engines with these terms
    #   b. possibly gather your own data set of person-interests pairs, use
    #      stats to add more interests or sort them better
    # 6. return both the list of all keywords and the list-of-lists categories
    return

# scraping linkedin.com/in/username
# usually of the form: firstname-lastname-[eight chars of some hash] but can be custom set
def scrape_linkedin(username):
    return

# scraping instagram.com/username
def scrape_instagram(username):
    return

# scraping twitter.com/username
def scrape_twitter(username):
    return

# scraping facebook.com/username
# usually of the form: firstname.lastname.number
def scrape_facebook(username):
    return

# scraping reddit.com/user/username
def scrape_reddit(username):
    return
