import hashlib
import sys

# TODO would this be more efficient if i returned the hex, the bytes, or converted
# to str? I suspect the str conversion is pointless
    # for example. could convert target hash to bytes and therefore do one
    # str conversion rather than one per guess
def hash(word, type):
    # TODO handle mistypings in type - ie, MD5 == md5 == md-5, etc
    # Constructors for hash algorithms that are always present in this module are
    # md5(), sha1(), sha224(), sha256(), sha384(), and sha512()
    if type == "md5":
        return hashlib.md5(bytes(word, 'utf-8')).hexdigest()
    if type == "sha1":
        return hashlib.sha1(bytes(word, 'utf-8')).hexdigest()
    if type == "sha224":
        return hashlib.sha224(bytes(word, 'utf-8')).hexdigest()
    if type == "sha256":
        return hashlib.sha256(bytes(word, 'utf-8')).hexdigest()
    if type == "sha384":
        return hashlib.sha384(bytes(word, 'utf-8')).hexdigest()
    if type == "sha512":
        return hashlib.sha512(bytes(word, 'utf-8')).hexdigest()
    # A generic new() constructor that takes the string name of the desired
    # algorithm as its first parameter also exists to allow access to the above
    # listed hashes as well as any other algorithms that your OpenSSL library
    # may offer. The named constructors are much faster than new() and should be
    # preferred.
    # TODO handle unsupported hash type error
    h = hashlib.new(type)
    h.update(word)
    return h.hexdigest()

# figure out what hash algo created the target hash
def determine_hash_type(target):
    # TODO determine type of hash from length, format, etc
    return
