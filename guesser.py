from hasher import hash

import string, time

# TODO write a kill on completion for finding successfully

# TODO maybe you can use recur_bf for other stuff by passing it a function as a
# param, using check_simple for wanting to brute check hashes, and some append function
# if you wanna use it for trying all appends of some word, etc
    # this also lets you try to use your pwd guessing for more than hash cracking
    # - if there's a vuln allowing you to mass guess pwd via post req, for example
    # TODO this can allow you to pass check_x that handles salt or uname:pwd inputs

# TODO convert "Trying x ..." in recur_bf and "on x" in g_c to percentage progress
# as seen in try_list

# TODO being able to pass a min char size to avoid repeats would be neat

# TODO is it generally a good design pattern to generate then check whole list,
# rather than check as you generate? Seems to add insignificant time while
# GREATLY consolidating code. But g_c is so much slower than recur_bf so idk
    # TODO i still have no fucking idea why g_c is so much slower than recur_bf
    # but i convered all funcs intended for brute forcing from "" to use recur_bf.
    # ones that append onto a prospective pw/prepend still use g_c. Do I modify
    # recur_bf to work for those ones too and risk breaking it, or do i fix g_c
    # to be as fast as recur_bf? only god knows now.

# TODO consider ways to maximise performance by giving higher-likely candidates
# priority in their lists

# --- Sec 1 of 5: core engines running the show --- #

# checks if input hashes to target
def check_simple(input, target, type):
    return str(hash(input, type)) == target

# given a str of possible chars, generates all combos of that str's chars up to char_lim
# TODO why is this so much slower than recur_bf? is it generating repeats on recur calls...?
# TODO you could save time and write combo outputs to a txt, keyed by available_chars and word_so_far.
#      save new ones, and no longer waste time redoing ones we've done. only reason
#      not to is if file size would be unmanageable but idk if it would
def generate_combos(char_lim, available_chars, word_so_far, progress_updates=True):
    if word_so_far == "" and progress_updates:
        start_time = time.time()
        print("Generating list of words of size " + str(char_lim) + " from dict " + available_chars)
    output = [word_so_far]
    if not len(word_so_far) == char_lim:
        for c in available_chars:
            if word_so_far == "" and progress_updates:
                print("on " + c)
            appended = generate_combos(char_lim, available_chars, word_so_far + c)
            for a in appended:
                if a not in output:
                    output.append(a)
    if word_so_far == "" and progress_updates:
        print("finished in " + str(time.time() - start_time) + " seconds")
    return output

# @Deprecated # TODO undeprecated lol
def recur_bf(word_so_far, target, type, char_lim, available_chars, progress_updates=True):
    if word_so_far == "" and progress_updates:
        start_time = time.time()
        print("Recursively making guesses up to size " + str(char_lim) + " from dict " + available_chars + "\n-------------------------")
    if check_simple(word_so_far, target, type):
        return word_so_far
    if len(word_so_far) == char_lim:
        return "Could not find a password of length " + str(char_lim) + " that hashes by " + type + " to " + target + " from dict " + available_chars
    for c in available_chars:
        # for progress updates
        if word_so_far == "" and progress_updates:
            print("Trying " + c + "...")
        # presumes char_lim <= length of err msg on line 72, and unless
        # quantum computing arrives, it better fuckin be lmao
        attempt = recur_bf(word_so_far + c, target, type, char_lim, available_chars)
        if len(attempt) <= char_lim:
            return "Password is: " + i + ". Found in " + str(time.time() - start_time) + " seconds"
    if word_so_far == "" and progress_updates:
        print("Finished in " + str(time.time() - start_time) + " seconds")
    return "Could not find a password of length " + str(char_lim) + " that hashes by " + type + " to " + target + " from dict " + available_chars

# given a list of pwds, tries all of them
def try_list(target, type, dictionary):
    start_time = time.time()
    print("Iterating through list with last entry: " + dictionary[len(dictionary) - 1] + "\n--------------")
    progress = 10
    for i in dictionary:
        if dictionary.index(i) >= (len(dictionary) / 100) * progress:
            print(str(progress) + "% completed")
            progress += 10
        if check_simple(i, target, type):
            return "Password is: " + i + ". Found in " + str(time.time() - start_time) + " seconds"
    return "100% completed. Could not crack " + target + " using inputs hashed via " + type + ". Executed in " + str(time.time() - start_time) + " seconds.\n--------------"

# --- Section 2 of 5: good ol fashioned brute force --- #

# try all combinations of typeable characters up to word_size
def brute_all(target, type, word_size):
    return recur_bf("", target, type, word_size, string.printable)

# try all combinations of alphanumeric characters up to word_size
def brute_alphanumeric(target, type, word_size):
    return recur_bf("", target, type, word_size, string.ascii_letters + string.digits)

# try all combinations of alphabetic characters up to word_size
def brute_alphabetic(target, type, word_size):
    return recur_bf("", target, type, word_size, string.ascii_letters)

# try all combinations of lowercase characters up to word_size
def brute_lowercase(target, type, word_size):
    return recur_bf("", target, type, word_size, string.ascii_lowercase)

# try all combinations of numeric characters up to word_size
def brute_numeric(target, type, word_size):
    return recur_bf("", target, type, word_size, string.digits)

# try all combinations of one Capital + lowercase characters up to word_size
def check_first_capital(target, type, word_size):
    inputs = []
    print("Generating list of inputs\n----------------")
    for c in string.ascii_uppercase:
        print("on " + c)
        for combo in generate_combos(word_size - 1, string.ascii_lowercase, "", False):
            inputs.append(c + combo)
    return try_list(target, type, inputs)

# TODO try all birthday combos
def brute_birthdays(target, type, word_size):
    return

# --- Section 3 of 5: modifying candidate pwds --- #

# try all combinations of input + typeable characters up to x
def check_appending(input, target, type, x):
    return check_appending_required(input, target, type, x, string.ascii_letters + string.punctuation + string.digits)

# try all combinations of input + non-letter-characters up to x
def check_appending_all_non_letters(input, target, type, x):
    return check_appending_required(input, target, type, x, string.punctuation + string.digits)

# try all combinations of input + common-non-letter characters up to x,
# including digits and common punctuation used in pwds according to:
#   TODO insert source
def check_appending_common_non_letters(input, target, type, x):
    return check_appending_required(input, target, type, x, string.digits + '.,-!?*();')

# try all combinations of input + numeric characters up to x
def check_appending_nums(input, target, type, x):
    return check_appending_required(input, target, type, x, string.digits)

# generate a list of possible birthyear combos of form 19xx, 20xx up to 2030, and xx
def generate_appending_birthyears(input):
    # this expires in 2030 lmao #y2k amirite
    just_decade_and_year = []
    twentieth = []
    twenty_first = []
    for combo in generate_combos(2, string.digits, ""):
        if len(combo) == 2:
            just_decade_and_year.append(input + combo)
            twentieth.append(input + "19" + combo)
            if (int(combo) <= 30):
                twenty_first.append(input + "20" + combo)
    return just_decade_and_year + twentieth + twenty_first

# try all combinations of input + birthyears
def check_appending_birthyears(input, target, type):
    return try_list(target, type, generate_appending_birthyears(input))

# TODO maybe this should take in a couple birth days to use rather than try and
# do all these...
# TODO implement
# generate all possible birthdays of form:
#   MMDD, MDD, MMDDYY, MMDDYY, MMDDYYYY, DDMMYY, DDMMYYYY,
#   DDMonthName, MonthNameDD, MonthNameDDYY, DDMonthNameYY, MonthNameDDYYYY, DDMonthNameYYYY
def generate_appending_birthdays(input):
    return

# try all combinations of input + birthday
def check_appending_birthdays(input, target, type):
    return try_list(target, type, generate_appending_birthdays(input))

# appends a list of given required chars
def check_appending_required(input, target, type, x, required):
    appended = [input]
    for combo in generate_combos(x, required, ""):
        appended.append(input + combo)
    return try_list(target, type, appended)

# prepends all possible combinations of up to x chars before input.
# checks if any of these hash to target. useful for salt
def check_prepending(input, target, type, x):
    prepended = [input]
    for combo in generate_combos(x, string.ascii_letters + string.punctuation + string.digits, ""):
        prepended.append(combo + input)
    return try_list(target, type, prepended)

# recursively generate possible leet combos of input
def generate_leets(input, combos_so_far):
    # TODO is this efficient? we generate a ton of already generated posses
    # TODO this should probably loop through a list of pairs of leet subs rather
    # than just be a long if-else
    # TODO consider removing some of the less likely leet substitutions?
    # TODO a version that only considers enough leet subs to satisfy req chars
    combos_so_far.append(input)
    for i in range(len(input)):
        if (input[i] == 'e'):
            prepend_normal = input[:i+1]
            prepend_leet = input[:i] + '3'
            post_leet_possibilities = generate_leets(input[i+1:], [])
            for poss in post_leet_possibilities:
                if not (prepend_normal + poss) in combos_so_far:
                    combos_so_far.append(prepend_normal+poss)
                if not (prepend_leet + poss) in combos_so_far:
                    combos_so_far.append(prepend_leet + poss)
    # TODO add even more leet possibilities
    return combos_so_far

# generates all possible combinations of 1337/leet versions of input
# checks if any of these hash to target
def check_leet(input, target, type):
    return try_list(target, type, generate_leets(input, []))

# TODO implement
# generate all possible capitalisation variations of a given word
def generate_all_cap_combos(word_so_far, combos_found):
    return

def check_all_capitalisation(input, target, type):
    return try_list(target, type, generate_all_cap_combos(input, []))

# TODO implement
# generate all likely capitalisation variations of a given word
# for example, if we detect the word to be two words adjoined, cap start of each one
def generate_likely_cap_combos(word_so_far, combos_found):
    return

def check_likely_capitalisation(input, target, type):
    return try_list(target, type, generate_likely_cap_combos(input, []))

# --- Section 4 of 5: Various guesses for each member of a list --- #

# given a list of pwds, tries all 'simple' variations on each individual pwd
def try_list_simple_variation(target, type, dictionary):
    # (TODO decide if this is statistically efficient)
    # TODO generate a list of:
    # 0. the original words
    # 1. all preprended versions of each word up to 3
    # 2. all appended versions of each word up to 4
    # and pass to b_f_s
    return

# given a list of pwds, tries all possible variations on each individual pwd
def try_list_complex_variation(target, type, dictionary):
    # (TODO decide if this is statistically efficient)
    # TODO generate list consisting of:
    # 0-2 list from b_f_s_v
    # 3. all leet speak versions of each word
    # 4. all leet speak and appended up to 3
    return

# given a list of pwds and list of required char types, tries tp mimick how a
# user squeezes required chars into their usual favourite pwd from candidates
def try_list_required_chars(target, type, dictionary):
    # TODO think about this lol
    return

# --- Section 5 of 5: Combining sections of a list --- #

# given a list of lists of words, tries to combine members of each list to make more
# complex ones (ie, maybe SportHometown or MomNameDadName)
def try_list_meta_simple(target, type, dictionary):
    # TODO
    return

# given a list of lists of words, tries to combine members of each list to make more
# complex ones as well as variations on the resultant combos
def try_list_meta_complex(target, type, dictionary):
    # TODO
    return
